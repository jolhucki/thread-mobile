const PostsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  REACT: '/react',
  EDIT: '/edit'
};

export { PostsApiPath };
