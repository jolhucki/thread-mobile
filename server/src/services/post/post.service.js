class Post {
  constructor({ postRepository, postReactionRepository }) {
    this._postRepository = postRepository;
    this._postReactionRepository = postReactionRepository;
  }

  getPosts(filter) {
    return this._postRepository.getPosts(filter);
  }

  getPostById(id) {
    return this._postRepository.getPostById(id);
  }  

  create(userId, post) {
    return this._postRepository.create({
      ...post,
      userId
    });
  }

  updatePost(user, id, body){
    return this._postRepository.updatePost(user, id, body);
  }

  async setReaction(userId, { postId, isDislike, isLike }) {
    // define the callback for future use as a promise
    const updateOrDelete = react => {
      if (react.isLike === isLike){
        console.log("Reaction id that will be deleted: " + react.id);
        this._postReactionRepository.deleteById(react.id);
      } else if (react.isLike !== isLike){
        this._postReactionRepository.updateById(react.id, { isLike, isDislike });
      }
      if (react.isDislike === isDislike){
        console.log("Reaction id that will be deleted: " + react.id);
        this._postReactionRepository.deleteById(react.id);
      } else if (react.isDislike !== isDislike){
        this._postReactionRepository.updateById(react.id, { isLike, isDislike });
      }
    };

    const reaction = await this._postReactionRepository.getPostReaction(
      userId,
      postId
    );

    const result = reaction
      ? await updateOrDelete(reaction)
      : await this._postReactionRepository.create({ userId, postId, isLike, isDislike });

    // the result is an integer when an entity is deleted
    return Number.isInteger(result)
      ? {}
      : this._postReactionRepository.getPostReaction(userId, postId);
  }

}

export { Post };
