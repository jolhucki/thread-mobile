const TableName = {
    POST_REACTIONS: 'post_reactions',
};

const ColumnName = {
    IS_DISLIKE: 'is_dislike',
    IS_LIKE: 'is_like'
};

const RelationRule = {
    CASCADE: 'CASCADE',
    SET_NULL: 'SET NULL'
};

export async function up(knex){
    await knex.schema.alterTable(TableName.POST_REACTIONS, table => {
        table.boolean(ColumnName.IS_LIKE).notNullable().defaultTo(false)
        table.boolean(ColumnName.IS_DISLIKE).notNullable().defaultTo(false)
    })
}

export async function down(knex){
}
