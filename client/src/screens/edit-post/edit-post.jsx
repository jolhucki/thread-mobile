import * as React from 'react';
import { TextInput } from 'react-native';
import {
    ButtonVariant,
    HomeScreenName,
    IconName,
    NotificationMessage, 
    TextVariant
} from 'common/enums/enums';
import { Button, Image, Text, View } from 'components/components';
import { AppColor } from 'config/config';
import { pickImage } from 'helpers/helpers';
import { useDispatch, useNavigation, useState } from 'hooks/hooks';
import {
  image as imageService,
  notification as notificationService
} from 'services/services';
import { threadActionCreator } from 'store/actions';
import styles from './styles';
import { useSelector } from 'hooks/hooks';

// accepts text of the post that was previously posted
const EditPost = (request) => {
    const dispatch = useDispatch();
    const navigator = useNavigation();

    const { postUser, postId, postBody } = request.route.params;
    
    const [body, setBody] = useState(postBody ? postBody : '');

    console.log("Data handled in the edit-pos.jsx");
    console.log(postUser);
    console.log(postId);
    console.log(postBody);
    console.log("END");
    
    const handlePostEdit = async () => {
        if (!body){
            return;
        }

        console.log("handlePostEdit was used");
        dispatch(threadActionCreator.editPost({ user: postUser.id, id: postId, body: body }));
        setBody('');
        navigator.navigate(HomeScreenName.THREAD);
    };

    return (
        <View style={styles.screen}>
            <Text variant={TextVariant.HEADLINE}>Edit post</Text>
            <TextInput 
                multiline
                value={body}
                placeholder='Type new text of the post...'
                placeholderTextColor={AppColor.PLACEHOLDER}
                numberOfLines={10}
                style={styles.input}
                onChangeText={setBody}
            />
            <Button
                title="Update"
                isDisabled={!body}
                onPress={handlePostEdit}
            />
        </View>
    )
}

export default EditPost;
